import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div>
      <nav className="mt-2">
        <Link to="/" className="btn btn-primary mx-1">
          Home
        </Link>
        <Link to="/game" className="btn btn-primary mx-1">
          Game
        </Link>
        <Link to="/score" className="btn btn-primary mx-1">
          score
        </Link>
      </nav>
    </div>
  );
}

export default Navbar;
