import React from "react";
import suitValue from "./suitValue";

const SuitChoicesSystem = ({ mainkan }) => {
  return (
    <ul>
      {suitValue.map((choice) => (
        <li
          key={choice.value}
          onClick={() => mainkan(choice.value)}
          style={{ cursor: "pointer" }}
        >
          {choice.value}
        </li>
      ))}
    </ul>
  );
};

export default SuitChoicesSystem;
