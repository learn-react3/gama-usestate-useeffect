import React, { createContext, useState } from "react";

export const SummaryContext = createContext({
  summary: [],
  setSummary: () => undefined,
});

const SumContext = ({ children }) => {
  const [summary, setSummary] = useState([]);

  return (
    <div>
      <SummaryContext.Provider value={{ summary, setSummary }}>
        {children}
      </SummaryContext.Provider>
    </div>
  );
};

export default SumContext;
