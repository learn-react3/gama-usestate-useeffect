import React, {
  useContext,
  useState,
  useEffect,
  useMemo,
  useCallback,
} from "react";
import "./styleGame.css";

import Score from "../score/Score";
import SuitChoicesSystem from "../../components/SuitChoice/SuitChoices";
import suitValue from "../../components/SuitChoice/suitValue";
import { SummaryContext } from "../../context/sumContext";

const Game = () => {
  // State
  const [userChoice, setUserChoice] = useState("");
  const [compChoice, setCompChoice] = useState("");
  const [winner, setWinner] = useState("");
  const [score, setScore] = useState({
    userScore: 0,
    compScore: 0,
  });
  const [date, setDate] = useState(new Date());
  const [dataChange, setDataChange] = useState(false); // Tambahan state agar bisa diklik berulang2
  const [game, setGame] = useState(1);

  // Context
  const { summary, setSummary } = useContext(SummaryContext);

  // Untuk mengupdate pilihan player dan set datachange menjadi true
  const mainkan = useCallback((choice) => {
    setUserChoice(choice);
    setDataChange(true);
  }, []);

  // Untuk mengupdate pilihan computer
  const getHasil = useMemo(() => {
    if (userChoice === compChoice) return "SERI";
    if (userChoice === "batu") return compChoice === "kertas" ? "COMP" : "USER";
    if (userChoice === "gunting")
      return compChoice === "kertas" ? "USER" : "COMP";
    if (userChoice === "kertas") return compChoice === "batu" ? "USER" : "COMP";
  }, [compChoice, userChoice]);

  // Untuk mengupdate score
  const getScore = useCallback(
    (a) => {
      if (a === "SERI")
        setScore({
          ...score,
          userScore: score.userScore + 0,
          compScore: score.compScore + 0,
        });
      if (a === "USER")
        setScore({
          ...score,
          userScore: score.userScore + 1,
        });
      if (a === "COMP")
        setScore({
          ...score,
          compScore: score.compScore + 1,
        });
    },
    [dataChange] //ini klo dirubah ngikutin eslint, scorenya jadi aneh
  );

  // Update summary
  const submitSummary = useCallback(() => {
    setGame(game + 1);
    setSummary([
      ...summary,
      {
        game: game,
        score: `${score.userScore} - ${score.compScore}`,
        winner: winner,
        date: date.toLocaleTimeString(),
      },
    ]);
  }, [
    date,
    game,
    score.compScore,
    score.userScore,
    setSummary,
    summary,
    winner,
  ]);

  // Hook comp turn
  useEffect(() => {
    const pilComp = suitValue[Math.floor(Math.random() * suitValue.length)];
    setCompChoice(pilComp.value);
    setDataChange(false);
  }, [dataChange]);

  // Hook hasil match
  useEffect(() => {
    setWinner(getHasil);
    setDate(new Date());
  }, [dataChange, getHasil]);

  // Hook set score
  useEffect(() => {
    getScore(winner);
  }, [date]); //ini klo dirubah ngikutin eslint, scorenya jadi aneh

  return (
    <div className="mt-3">
      <SuitChoicesSystem mainkan={mainkan} />
      <button onClick={submitSummary} className="btn btn-warning">
        submit
      </button>
      <br />
      <Score />
    </div>
  );
};

export default Game;
