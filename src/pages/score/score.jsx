import React, { useContext } from "react";
import { SummaryContext } from "../../context/sumContext";

import "./Score.css";

function Score() {
  const { summary } = useContext(SummaryContext);

  if (summary.length === 0) {
    return <div>Anda Belum Main</div>;
  }
  return (
    <div>
      <table>
        <tr>
          <th>Game</th>
          <th>Winner</th>
          <th>Score</th>
          <th>Date</th>
        </tr>
        {summary.map((item, index) => (
          <tr key={index}>
            <td>{item.game}</td>
            <td>{item.winner}</td>
            <td>{item.score}</td>
            <td>{item.date}</td>
          </tr>
        ))}
      </table>
    </div>
  );
}

export default Score;
