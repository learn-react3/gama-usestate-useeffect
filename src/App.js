import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/home/Home";
import Game from "./pages/game/Game";
import Score from "./pages/score/Score";

import Navbar from "./components/navbar/Navbar";
import SummaryContext from "./context/sumContext";

function App() {
  return (
    <div className="col-5">
      <SummaryContext>
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/game" component={Game} />
            <Route path="/score" component={Score} />
          </Switch>
        </Router>
      </SummaryContext>
    </div>
  );
}

export default App;
